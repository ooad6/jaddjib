import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Order from "@/types/Order";
import orderService from "@/service/order";
import { useMessageStore } from "./message";
import type orderItem from "@/types/OrderItem";
import { useLoadingStore } from "./loading";
export const useOrderStore = defineStore("order", () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const orderList = ref<Order[]>([]);
  const finshedList = ref<orderItem[]>([]);
  const selectedList = ref<orderItem[]>([]);
  const orderItemList = ref<orderItem[]>([]);
  const orderSelected = ref();
  const orderIdSelected = ref();

  async function getOrders() {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getOrders();
      orderList.value = res.data;
      for (const i in orderList.value) {
        for (const j in orderList.value[i].orderItems) {
          orderItemList.value.push(orderList.value[i].orderItems[j]);
        }
      }
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
    loadingStore.isLoading = false;
  }

  // const totalPrice = computed(() => {
  //   return orderList.value.reduce((total, price) => {
  //     return total;
  //   }, 0);
  // });

  const deleteOrder = (id: string): void => {
    const index = orderList.value.findIndex((item) => item.id === id);
    orderList.value.splice(index, 1);
  };

  const finshedOrder = (id: number): void => {
    //const index = orderList.value.findIndex((item) => item.id === id);
    const selectedindex = selectedList.value.findIndex(
      (item) => item.id === id
    );
    finshedList.value.push(selectedList.value[selectedindex]);
    selectedList.value.splice(selectedindex, 1);
  };

  const selectedOrder = (id: string): void => {
    const index = orderList.value.findIndex((item) => item.id === id);
    selectedList.value.push(orderItemList.value[index]);
    deleteOrder(id);
  };

  async function getOrderById(id: string) {
    try {
      const res = await orderService.getOrderById(id);
      const order = res.data;
      return order;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get menu information from database.");
    }
  }

  async function getOrderItemByOrderId(id: string) {
    try {
      const res = await orderService.getOrderById(id);
      const orderItmeList = ref<orderItem[]>([]);
      orderItmeList.value = res.data[0].orderItems;
      // console.log(order.value);
      return orderItmeList.value;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get menu information from database.");
    }
  }

  async function getOrderByTableNumber(table_number: number) {
    try {
      const res = await orderService.getOrderByTableNumber(table_number);
      return res.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
  }

  const createOrder = async (order: Order) => {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.saveOrder(order);
      return res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  };
  const updateOrderTableNumber = async (table_number: number) => {
    loadingStore.isLoading = true;
    try {
      orderSelected.value = await getOrderById(orderIdSelected.value + "");
      orderSelected.value[0].table_number = table_number;
      orderSelected.value[0].orderItems = [];
      console.log(orderIdSelected.value);
      console.log(orderSelected.value[0]);
      deleteOrder(orderIdSelected.value);

      const res = await orderService.updateOrder(
        orderIdSelected.value,
        orderSelected.value[0]
      );
      console.log(orderSelected.value!);

      console.log(res.data);
    } catch (e) {
      console.log(e);
    }
    await getOrders();
    loadingStore.isLoading = false;
  };

  // const deleteOrderItem = (id: number) => {
  //   orderitemId
  //   orderService.getOrderItems(id);
  // };

  return {
    orderList,
    // totalPrice,
    deleteOrder,
    finshedOrder,
    finshedList,
    selectedOrder,
    selectedList,
    getOrders,
    orderItemList,
    getOrderById,
    createOrder,
    getOrderByTableNumber,
    orderSelected,
    updateOrderTableNumber,
    orderIdSelected,
    getOrderItemByOrderId,
  };
});
