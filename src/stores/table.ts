import { ref } from "vue";
import { defineStore } from "pinia";
import type Table from "@/types/Table";
import tableService from "@/service/table";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useTableStore = defineStore("table", () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const selected = ref<Table>({});
  const dialog = ref(false);
  const dialogTable = ref(false);
  const showBtnQRCode = ref(true);
  const tableList = ref<Table[]>([]);
  const oldTable = ref(-1);
  const dialogEdit = ref(false);
  const editedTable = ref<Table>({
    seats: 0,
    status: "A",
  });

  async function getTables() {
    loadingStore.isLoading = true;
    try {
      const res = await tableService.getTables();
      tableList.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get table information from database.");
    }
    loadingStore.isLoading = false;
  }

  const selectTable = (table: Table): void => {
    selected.value = table;
    console.log(selected.value);
  };

  const updateTable = async (status: string) => {
    loadingStore.isLoading = true;
    try {
      selected.value.status = status;
      const res = await tableService.updateTable(selected.value);
      console.log(res.data);
    } catch (e) {
      console.log(e);
    }
    dialog.value = false;
    await getTables();
    loadingStore.isLoading = false;
  };

  const confirmClear = async (): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      selected.value.status = "A";
      const res = await tableService.updateTable(selected.value);
      console.log(res.data);
    } catch (e) {
      console.log(e);
    }
    dialog.value = false;
    await getTables();
    loadingStore.isLoading = false;
  };

  const cancel = (): void => {
    dialog.value = false;
    selected.value = {};
  };

  const clear = () => {
    editedTable.value = {
      seats: 0,
      status: "A",
    };
  };

  const editTable = (table: Table) => {
    dialogEdit.value = true;
    editedTable.value = { ...table };
  };

  const saveTable = async () => {
    loadingStore.isLoading = true;
    try {
      if (editedTable.value.id) {
        //edt id not empty = edt
        const res = await tableService.updateTable(editedTable.value);
        console.log(res);
      }
    } catch (e) {
      console.log(e);
    }
    await getTables();
    dialogEdit.value = false;
    clear();
    loadingStore.isLoading = false;
  };

  return {
    tableList,
    selectTable,
    selected,
    dialog,
    confirmClear,
    cancle: cancel,
    getTables,
    updateTable,
    dialogTable,
    showBtnQRCode,
    oldTable,
    dialogEdit,
    editTable,
    clear,
    saveTable,
    editedTable,
  };
});
