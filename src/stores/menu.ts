import { ref } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";
import menuService from "@/service/menu";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useMenuStore = defineStore("menu", () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const menuList = ref<Menu[]>([]);
  const selectedMenu = ref<Menu>();
  const selected = ref<Menu>();
  const dialogEdit = ref(false);
  const editedMenu = ref<Menu>({
    name: "",
    price: 0,
    img: "",
  });

  async function getMenus() {
    loadingStore.isLoading = true;
    try {
      const res = await menuService.getProducts();
      menuList.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get menu information from database.");
    }
    loadingStore.isLoading = false;
  }

  async function getMenuById(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await menuService.getProductsById(id);
      return res.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get menu information from database.");
    }
    loadingStore.isLoading = false;
  }

  const clear = () => {
    editedMenu.value = {
      name: "",
      price: 0,
      img: "",
    };
  };

  const editMenu = (menu: Menu) => {
    dialogEdit.value = true;
    editedMenu.value = { ...menu };
  };

  const saveMenu = async () => {
    loadingStore.isLoading = true;
    try {
      if (editedMenu.value.id) {
        //edt id not empty = edt
        const res = await menuService.updateProduct(
          editedMenu.value.id,
          editedMenu.value
        );
        console.log(res);
      } else {
        //new
        const res = await menuService.saveProduct(editedMenu.value);
        console.log(res);
      }
    } catch (e) {
      console.log(e);
    }
    await getMenus();
    dialogEdit.value = false;
    clear();
    loadingStore.isLoading = false;
  };

  const deleteMenu = async (id: number): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await menuService.deleteProduct(id);
      console.log(res);
    } catch (e) {
      console.log(e);
    }
    await getMenus();
    loadingStore.isLoading = false;
  };

  return {
    menuList,
    getMenus,
    selectedMenu,
    getMenuById,
    selected,
    editMenu,
    saveMenu,
    editedMenu,
    dialogEdit,
    clear,
    deleteMenu,
  };
});
