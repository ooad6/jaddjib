import type Reciept from "@/types/Receipt";
import http from "./axios";

function getReciepts() {
  return http.get("/receipts");
}

function saveReceipt(receipt: Reciept) {
  return http.post("/receipts", receipt);
}

function updateReciept(id: string, receipt: Reciept) {
  return http.patch(`/receipts/${id}`, receipt);
}

function deleteReciept(id: string) {
  return http.delete(`/receipts/${id}`);
}

function getRecieptById(id: string) {
  return http.get(`/receipts/${id}`);
}

export default {
  saveReceipt,
  getRecieptById,
  getReciepts,
  updateReciept,
  deleteReciept,
};
