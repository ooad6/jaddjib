import type Order from "@/types/Order";
import http from "./axios";

function getOrders() {
  return http.get("/orders");
}

function getOrderItems() {
  return http.get("/orders");
}

function saveOrder(order: Order) {
  return http.post("/orders", order);
} //new

function updateOrder(id: string, order: Order) {
  return http.patch(`/orders/${id}`, order);
}

function deleteOrder(id: string) {
  return http.delete(`/orders/${id}`);
}

function deleteOrderItem(id: number) {
  console.log(id);
  return http.delete(`/orders/item/${id}`);
}

function getOrderById(id: string) {
  return http.get(`/orders/${id}`);
}

function getOrderByTableNumber(table_number: number) {
  return http.get(`/orders/table_number/${table_number}`);
}

export default {
  getOrders,
  saveOrder,
  updateOrder,
  deleteOrder,
  getOrderItems,
  getOrderById,
  getOrderByTableNumber,
  deleteOrderItem,
};
