export default interface Table {
  id?: number;
  seats?: number;
  status?: string;
}
